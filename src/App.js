import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGitlab } from '@fortawesome/free-brands-svg-icons'
import "./remix-api";
import { Message, Form, TextArea, Header, Image } from 'semantic-ui-react'
// Radio, Popup, Icon, Menu, Segment
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import JSONTree from 'react-json-view'
import { Helmet } from 'react-helmet'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'
import { escrow } from './example-contracts'
import * as wrapper from 'solc/wrapper';

import { createIframeClient } from '@remixproject/plugin'
import Button from 'react-bootstrap/Button'
//import { CompilationResult, editor, compile, Api, Status, HighlightPosition, RemixApi } from '@remixproject/plugin'

// Components
// import CompilerButton from './components/CompilerButton'
import './App.css'

// import lexon
//const lexon = import("lexon-wasm");
// won't work if no net connection as solc and version list is loaded remotely
// makes sure solc is also available in browser
const solc = wrapper(window.Module);
// creates remix/iframe client
const extension = createIframeClient()


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      solidity_results: '',
      lexon_contract: escrow,
      filename: `browser/${escrow.name}`,
      placeholderText: 'Code *.lex',
      loading: false,
      sol: '',
      compileDst: "remote",
      compilationResult: {
        status: false,
        message: '',
        sol: '',
        lexon_contract: escrow,
        contractName: '',
        bytecode: '',
        bytecode_runtime: '',
        abi: '',
        ir: '',
        methodIdentifiers: ''
      },
      menu: {
        active: 'lexon_contract'
      },
      copied: false
    }

    this.onCompileFromRemix = this.onCompileFromRemix.bind(this)
    this.onCompileSucceeded = this.onCompileSucceeded.bind(this)
    this.highlightErrors = this.highlightErrors.bind(this)
    this.onCompileFailed = this.onCompileFailed.bind(this)
    this.onPluginLoaded = this.onPluginLoaded.bind(this)
    this.onClickTab = this.onClickTab.bind(this)
    this.onPluginLoaded();
  };

    async onPluginLoaded() {
        await extension.onload(() =>
            extension.call('fileManager', 'setFile', `browser/${escrow.name}`, escrow.content)
        )
        await extension.onload(() => extension.call('fileManager', 'switchFile', `browser/${escrow.name}`, ))
    }

  async onCompileFromRemix() {
    this.setState({ compilationResult: {status: "inProgress" }})
    const plugin = this
    plugin.result = {}
    const fileName = await extension.call('fileManager', 'getCurrentFile')
    this.setState({filename: fileName})
    // (error, result) => {
      plugin.result.placeholderText = fileName
      plugin.result.copied = false
      // (error, result) => { 
        const fileContent = await extension.call('fileManager', 'getFile', fileName)
        this.setState({ lexon_contract: fileContent })
        console.log(fileContent)
        plugin.result.lexon = fileName
        extension.emit('statusChanged', ({
                key: 'loading',
                type: 'info',
                title: 'Compiling'
            }))
        plugin.setState(plugin.result)
        plugin.compile(plugin.onCompileSucceeded, plugin.onCompileFailed, plugin.result)
  }

    compile(onCompileSucceeded, onCompileFailed, result) {
            if (!this.state.placeholderText) {
                onCompileFailed({
                    status: 'failed',
                    message: "Set your Lexon contract file."
                })
                return
            }
            const fileExtension = this.state.placeholderText.split('.')[1]
            if (fileExtension !== 'lex') {
                onCompileFailed({
                    status: 'failed',
                    message: "Use extension .lex for Lexon."
                })
                return
            }
  
        var input = {
            language: 'Solidity',
            sources: {
                [this.state.filename]: {
                    content: ''
                }
            },
            settings: {
                outputSelection: {
                    '*': {
                        '*': ['*']
                    }
                }
            }
        };

        var compile_results = {
            lexon_contract: this.state.lexon_contract,
            placeholderText: 'Code *.lex',
            loading: false,
            sol: '',
            compileDst: "remote",
            compilationResult: {
                status: 'success',
                message: '',
                sol: '',
                contractName: '',
                bytecode: '',
                bytecode_runtime: '',
                abi: '',
                ir: '',
                methodIdentifiers: ''
            },
            menu: {
                active: 'lexon_contract'
            },
            copied: false
        }
        if (this.state.wasm) {
            
            let error = this.state.wasm.compile(this.state.lexon_contract);
            let solidity = error;
            console.log(error);
            if (error === "ok") {
                solidity = this.state.wasm.solidity()
                input['sources'][this.state.filename]['content'] = solidity;
                var lex_results = input['sources'][this.state.filename]['content']
                compile_results.sol = lex_results
                compile_results.compilationResult.sol = lex_results

                if (solc) {
                var solc_results = solc.compile(JSON.stringify(input))
                this.setState({solidity_results: JSON.parse(solc_results)})
                console.log("solidity results state ", this.state.solidity_results)
            } else {
                onCompileFailed(error, this.state.filename)
            }
            // this gets the contract name to access the json. currently only built for 1
            var i = 0
            var contract_name
            for (var property in JSON.parse(solc_results)['contracts'][this.state.filename]) {
                i += 1
                if (i > 1) {
                    console.log('Error, so far only single contracts are supported')
                } else {
                    console.log("contract name is ", property); // Outputs: foo, fiz or fiz, foo
                    contract_name = property
                    //this.setState({ compilationResult: {contractName: contract_name }})
                }
            }
            this.setState({
                compilationResult: {
                    status: "success",
                    message: '',
                    sol: lex_results,
                    contractName: contract_name,
                    bytecode: JSON.parse(solc_results)['contracts'][this.state.filename][contract_name]['evm']['bytecode'],
                    bytecode_runtime: JSON.parse(solc_results)['contracts'][this.state.filename][contract_name]['evm']['bytecode_runtime'],
                    abi: JSON.stringify(JSON.parse(solc_results)['contracts'][this.state.filename][contract_name]['abi']).replace(/\n/g, '').replace(/\\t/g, ''),
                    ir: '',
                    methodIdentifiers: JSON.parse(solc_results)['contracts'][this.state.filename][contract_name]['evm']['methodIdentifiers']
                }
            })
            extension.emit('compilationFinished', this.state.filename, this.state.lexon_contract, 'lexon', JSON.parse(solc_results))
            // console.log(this.state.compilationResult)
        // passing compilation to compileSucceeded
        console.log('filename is', this.state.filename)
        onCompileSucceeded(lex_results, solc_results, result.placeholderText)
            } else {
                onCompileFailed(error, this.state.filename)
            }
        }
        


            
    }

  highlightErrors(fileName, lineColumnPos, color) {
    const obj = [lineColumnPos, fileName, color]
    console.log(obj)
    extension.call('editor', 'highlight', lineColumnPos, fileName, color)
  }

  onCompileFailed(compileResults, fileName) {
    // replace with this.state.filename
    // add additional error message
    extension.emit('statusChanged', ({
        key: 'failed',
        type: 'error',
        title: 'Compilation Error'
      }))
    console.log('compileResults ', compileResults)
    if(fileName && compileResults) {
      let cr = JSON.stringify(compileResults)
      let p = cr.match(/-->\s?(\d*):(\d*)(?:(?:.|\n)*)=\s(.*)/i)
      console.log(p)
      const line = p.length === 4 ? p[1] : 0
      console.log('line', line)
      const col = p.length === 4 ? p[2] : 0
      let position = {start: {line: parseInt(line)-1, column: parseInt(col)-1}, end: {line: parseInt(line)-1, column: parseInt(col)} }
      this.highlightErrors(fileName, position, '#e0b4b4')
    }
  }

  onCompileSucceeded(lexResults, solcResults, fileName) {
    //console.log(lexResults)
    //console.log(solcResults)
    //console.log(fileName)
    // maybe also add this.state.filename
    //extension.call('editor', 'discardHighlight', [], (error, result) => {})
    var abi = JSON.parse(solcResults)['contracts'][this.state.filename][this.state.compilationResult.contractName]['abi']
    console.log(JSON.parse(solcResults)['contracts'][this.state.filename][this.state.compilationResult.contractName]['abi'])
    var bytecode = JSON.stringify((JSON.parse(solcResults)['contracts'][this.state.filename][this.state.compilationResult.contractName]['evm']['bytecode']))
    //.replace('0x','')
    var deployedBytecode = JSON.stringify(JSON.parse(solcResults)['contracts'][this.state.filename][this.state.compilationResult.contractName]['evm']['deployedBytecode']['object'])
    //console.log('deployedBytecode ', deployedBytecode.length, deployedBytecode.indexOf('_'))
    // byteCodeToDeploy apparently not used...
    var bytecodeToDeploy = bytecode.object;
    // console.log(bytecodeToDeploy.indexOf('_') >= 0)
    var methodIdentifiers = (JSON.parse(solcResults)['contracts'][this.state.filename][this.state.compilationResult.contractName]['evm']['methodIdentifiers'])
    var opcodes = JSON.stringify(JSON.parse(solcResults)['contracts'][this.state.filename][this.state.compilationResult.contractName]['evm']['deployedBytecode']['opcodes'])
    //.replace(/0x/g,'')
    var data = {

      'sources': {},
      'contracts': {}
    }
    data['sources'][this.state.placeholderText] = { id: 1, ast: {} }
    data['contracts'][this.state.placeholderText] = {}
    console.log("sliced filename ", data['contracts'][this.state.placeholderText][this.state.placeholderText.split('/').slice(-1)[0].split('.')[0]])
    // If the language used has no contract names, this field should equal to an empty string.
    data['contracts'][this.state.placeholderText][this.state.placeholderText.split('/').slice(-1)[0].split('.')[0]] = {
      // The Ethereum Contract ABI. If empty, it is represented as an empty array.
      // See https://github.com/ethereum/wiki/wiki/Ethereum-Contract-ABI
      "abi": abi,
      "sol": lexResults,
      "evm": {
        "bytecode": {
          "linkReferences": {

          },
          "object": bytecode,
          "opcodes": opcodes,
        },
        "deployedBytecode": {
          "linkReferences": {

          },
          "object": deployedBytecode,
          "opcodes": ""
        },
        "methodIdentifiers": methodIdentifiers
      }
    }
    /** Emit an event to Remix with compilation result */
    // console.log("lexon contract " , this.state.lexon_contract)
    // maybe also replace placeholdeText with fileName?
    //console.log(solcResults)
    //extension.emit('compilationFinished', this.state.placeholderText, this.state.lexon_contract, 'lexon', solcResults)
    console.log('emitting earlier')
    extension.emit('statusChanged', ({
        key: 'succeed',
        type: 'success',
        title: 'Compilation finished'
      }))
  }

  createCompilationResultMessage(fileName, result) {
    console.log("status is ", result.status)
    if(result.status === 'success') {
     /* , null , "\t") */
      return {
        bytecode: JSON.stringify(this.state.compilationResult['bytecode']),
        // this.state.compilationResult['bytecode'],
        bytecode_runtime: this.state.compilationResult['bytecode_runtime'],
        abi: JSON.stringify(this.state.compilationResult['abi'], '\t', 0),
        ir: this.state.compilationResult['ir'],
        solidity_code: this.state.compilationResult['sol'],
        lexon_contract: this.state.lexon_contract
      }

    } else if(result.status === 'failed' && result.column && result.line) {
      const header = `${fileName}:${result.line}:${result.column}`
      const body = this.state.compilationResult.message.split(/\r\n|\r|\n/)
      const arr = [header].concat(body).join("\n")
      this.setState({ result: {status: "" }})
      return {
        bytecode: arr,
        bytecode_runtime: arr,
        abi: arr,
        ir: arr,
        solidity_code: arr
      }
    } else if(result.status === 'failed') {
      const message = this.state.compilationResult.message
      return {
        bytecode: message,
        bytecode_runtime: message,
        abi: message,
        ir: message,
        solidity_code: message
      }
    }
    return {
      bytecode: "",
      bytecode_runtime: "",
      abi: "",
      ir: "",
      solidity_code: ""
    }

  }

/** Update the status of the plugin in remix */
  changeStatus(status) {
    extension.emit('statusChanged', status);
  }

/* Imports lexon wasm */
componentDidMount(){
    if(!this.state.wasm)
    {
      import('lexon-wasm').then((wasm)=>{
          this.setState({wasm:new wasm.LexonWasm()});
          console.log("Lexon Wasm loaded", wasm.LexonWasm)
        }

      )
    }
  }

  onClickTab(e, {name}) {
    console.log('e ', e)
    console.log('name ', name)
    // why is e correct, but name empty?
    this.setState({menu: {active: e}})
  }

  renderBytecode(message) {
    return (
      <Message><pre className="bytecode">{message}</pre></Message>
    )
  }

  renderText(message) {
    return (
      <Form><Form.TextArea value={message} rows={20}/></Form>

    )
  }

  renderCompilationResult(fileName, result) {
    const activeItem = this.state.menu.active
    console.log("activeItem ", activeItem)
    const message = this.createCompilationResultMessage(fileName, result);
    console.log("message ", message)
    {/* console.log(this.state) */}
    if (this.state.compilationResult.status !== "success") return (
      <div id="result">
      <p>No contract compiled yet.</p>
            <Button variant="primary" Content='Create Example' icon='file' disabled={this.state.loading} onClick={this.onCompileFromRemix}>Create Example</Button>
        Create escrow.lex example
    </div>); else 

    return (
    <div id="result">
        <Tabs id="result" activeKey={activeItem} onSelect={this.onClickTab}>
      <Tab eventKey="lexon_contract" title="Lexon Contract">
        <CopyToClipboard text={message["lexon_contract"]}>
          <Button variant="info" className="copy">Copy Lexon Contract</Button>
        </CopyToClipboard>
        <textarea defaultValue={message["lexon_contract"]}></textarea>
      </Tab>
      <Tab eventKey="solidity_code" title="Solidity Code">
        <CopyToClipboard text={message["solidity_code"]}>
          <Button variant="info" className="copy">Copy Solidity Code</Button>
        </CopyToClipboard>
        <textarea defaultValue={message["solidity_code"]}></textarea>
      </Tab>
      <Tab eventKey="bytecode" title="bytecode">
        <CopyToClipboard text={JSON.stringify(this.state.solidity_results['contracts'][this.state.filename][this.state.compilationResult.contractName]['evm']['bytecode'])}>
          <Button variant="info" className="copy">Copy bytecode</Button>
        </CopyToClipboard>
        <JSONTree src={this.state.solidity_results['contracts'][this.state.filename][this.state.compilationResult.contractName]['evm']['bytecode']} />
      </Tab>
      <Tab eventKey="abi" title="ABI">
        <CopyToClipboard text={message["abi"]}>
          <Button variant="info" className="copy">Copy ABI</Button>
        </CopyToClipboard>
        <textarea defaultValue={message["abi"]}></textarea>
      </Tab>
    </Tabs></div>
    )
  }

  render() {
    return (
    <main id="lexon-plugin">
      
          <header className="bg-light">
          <div className="title">
            <Image src="./logo.svg" alt="Lexon logo" className="logo" />
            <h4>Lexon</h4>
            </div>
            
            <a href="https://gitlab.com/lexonfoundation/lexon-remix" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon icon={faGitlab} />
            </a>
            
          </header>


          
          
          <div id="compile-btn">
            <Button variant="primary" Content='Compile' icon='sync' disabled={this.state.loading} onClick={this.onCompileFromRemix}>Compile {this.state.filename}</Button>
          </div>
          <section>
          <article id="result">
         
            {this.renderCompilationResult(this.state.placeholderText, this.state.compilationResult)}
            
          </article>
      </section>
      </main>
    );
  }
}

export default App;
